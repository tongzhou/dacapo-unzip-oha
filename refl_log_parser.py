#!/usr/bin/env python
import sys

def parse(bench):
    nfn = 'bms/%s/refl.log' % bench
    ofs = open(nfn, 'w')
    for line in open('scripts/out/refl.log'):

        if not line.startswith('Class.forName')\
          and not line.startswith('Class.newInstance')\
          and not line.startswith('Method.invoke')\
          and not line.startswith('Constructor.newInstance'):          
            continue
        
        if 'OHAHelper' in line:
            continue

        if 'sun.reflect.' in line:
            continue

        if 'sun.security.provider.Sun' in line:
            continue

        if 'sun.security.rsa.SunRsaSign' in line:
            continue

        if 'sun.text.resources.en.FormatData_en_AU' in line:
            continue

        if 'java.awt.GraphicsEnvironment$$Lambda$1.960604060' in line\
           or 'sun.java2d.Disposer$$Lambda$2.96639997' in line\
           or 'sun.nio.fs' in line\
           or 'java.lang.invoke.' in line\
           or 'org.apache.xerces.' in line\
           or 'org.apache.commons.logging.LogFactory' in line\
           or 'org.apache.xmlgraphics.image.' in line\
           or 'org.apache.batik.' in line\
           or 'org.apache.xml.serializer.OutputPropertiesFactory$1' in line:
            continue
        ofs.write(line)

args = sys.argv         
assert len(args) == 2
bench = args[1]
parse(bench)
