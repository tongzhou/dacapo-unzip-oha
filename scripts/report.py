import subprocess
import gg
import os
import numpy as np


def getCounterStats():
    for b in ['avrora', 'fop', 'pmd', 'luindex', 'sunflow', 'lusearch',
              'xalan', 'h2']:
        print(b)
        countersFile = gg.getBenchDir(b) + 'iterCounts.txt'
        ls = getCounters(countersFile)
        counts = np.array(ls)
        print(' '.join(str(x) for x in ls))
        c1 = counts[12:20]
        c2 = counts[22:30]
        c3 = counts[32:40]
        c4 = counts[42:50]
        mean1 = np.mean(c1)
        std1 = np.std(c1)
        print(mean1, std1, mean1/mean1)
        
        mean2 = np.mean(c2)
        std2 = np.std(c2)
        print(mean2, std2, mean2/mean1)

        mean3 = np.mean(c3)
        std3 = np.std(c3)
        print(mean3, std3, mean3/mean1)

        mean4 = np.mean(c4)
        std4 = np.std(c4)
        print(mean4, std4, mean4/mean1)


def getCounters(fn):
    counts = []
    for line in open(fn):
        count = line.strip().split(': ')[1]
        counts.append(int(count))

    assert len(counts) >=30, 'counts len: %d' % len(counts)
    return counts

if __name__ == "__main__":
    getCounterStats()
