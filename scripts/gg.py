import platform

java = '/usr/lib/jvm/java-1.8.0-openjdk-amd64/bin/java'
j9 = '/home/tong/projects/openj9-projects/openj9-openjdk8-oha//build/linux-x86_64-normal-server-release/images/j2re-image/bin/java'
bench = ''
bms = ['luindex']
#bms = ['pmd', 'fop', 'luindex', 'lusearch', 'xalan']
allBms = ['avrora', 'pmd', 'fop', 'luindex', 'lusearch', 'xalan', 'sunflow', 'h2']
eaJar = 'ea-jars/new-ea.jar'
poaJar = 'ea-jars/poa-2.0.3.jar'
fake = False
useEA = True

if platform.system() == 'Darwin':
    java = 'java'


def sb(benches):
    global bms
    if isinstance(benches, str):
        benches = [benches]

    bms = benches    


def printBenches():
    print(bms)


def setAllBenches():
    global bms
    bms = allBms


def removeBench(bench):
    global bms
    newbms = []
    for b in bms:
        if bench in b:
            continue
        newbms.append(b)
    bms = newbms
    

def printAllBenches():
    print(allBms)


def printCmdOnly(v=True):
    global fake        
    fake = bool(v)


def getProjectDir():
    return '../'


def getBenchDir(b):
    return getProjectDir() + 'bms/' + b + '/'
