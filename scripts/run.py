import subprocess
import gg
import os



def runj9(n=1, ils=0, jopts="", jit="", env=None, runs=1, usePoa=False, useEA=True):
    gg.java = gg.j9
    for i in range(runs):
        run(n, ils, jopts, jit, env, usePoa, useEA)


def run(n=1, ils=0, jopts="", jit="", env=None, usePoa=False, useEA=True):
    compileOHAHelper()
    if isinstance(ils, int):
        ils = [ils]
        
    for bench in gg.bms:
        for i in ils:            
            makeCnf(bench, i)
            cmd = [gg.java, '-cp', '..:../harness:' + '../'+gg.eaJar]
            if usePoa:
                cmd += ['-javaagent:' + '../'+gg.poaJar]
            if jopts:
                cmd += [jopts]
            if jit:
                cmd += ['-Xjit:%s' % jit]
            cmd += ['Harness', '-c', 'OHAHelper']
            cmd += ['-v', bench, '-n', str(n)]
            print(' '.join(cmd))
            
            if gg.fake:
                continue

            env = {}
            if useEA:
                env = {
                    "OHA_useEA": "1",
                    "OHA_interThresh": '30',
                    "OHA_interBBThresh": '40',
                    "OHA_cng": gg.getBenchDir(bench) + "cng.txt",
                    "OHA_metadata": gg.getBenchDir(bench) + "metadata.txt"
                    }
            env["OHA_benchDir"] = gg.getBenchDir(bench)

            print(env)    
            if env is None:
                ret = subprocess.run(cmd)
            else:
                ret = subprocess.run(cmd, env=env)
            code = ret.returncode    
            
            if code != 0:
                print("return code:", code)
                print("failed cmd (cmd cannot contain empty str):", cmd)


def runAll(n=1, ils=0):
    gg.setAllBenches()
    run(n, ils)


def runPoa(n=1, jopts="", jit="", env=None):
    gg.useEA = False
    compileOHAHelper()
        
    for bench in gg.bms:
        for i in [0]:            
            makeCnf(bench, i)
            cmd = [gg.java, '-cp', '..:../harness:' + '../'+gg.eaJar]            
            cmd += ['-javaagent:' + '../'+gg.poaJar]
            if jopts:
                cmd += [jopts]
            if jit:
                cmd += ['-Xjit:%s' % jit]
            cmd += ['Harness', '-c', 'OHAHelper']
            cmd += ['-v', bench, '-n', str(n)]
            print(' '.join(cmd))
            
            if gg.fake:
                continue


            env = {
                "OHA_benchDir": gg.getBenchDir(bench),
            }
            
            ret = subprocess.run(cmd, env=env)
            code = ret.returncode    
            
            if code != 0:
                print("return code:", code)
                print("failed cmd (cmd cannot contain empty str):", cmd)

            cmd = 'python refl_log_parser.py ' + bench
            print(cmd)
            ret = subprocess.run(cmd.split(), cwd='..')
            print("return code:", ret.returncode)
    gg.useEA = True            
    

def doProfileRun():
    gg.useEA = False
    for bench in gg.bms:
        run(ils=0, n=50, usePoa=True)

    gg.useEA = True    

def compileOHAHelper():
    cwd = os.getcwd()
    os.chdir(gg.getProjectDir())
    os.system('javac -cp ea-jars/new-ea.jar:harness harness/OHAHelper.java')
    os.chdir(cwd)
    print('compiled OHAHelper, cwd:', cwd)

def buildBench(bench, ilevel, opts=None):
    cmd = [gg.java, '-ea', '-jar', 'ea-jars/new-ea.jar', '-bench', bench, '-ilevel', str(ilevel)]
    if opts:
        cmd += opts
    print("cmd:", cmd)
    ret = subprocess.run(cmd, cwd='..')
    print("return code:", ret.returncode)
    print()


def build(ils=None, opts=None):
    bms = gg.bms
    if type(bms) == str:
        bms = [bms]
    if ils is None:
        ils = (0, 1, 2)

    for bench in bms:
        for il in ils:
            buildBench(bench, il, opts)


def buildAll():
    gg.setAllBenches()
    build()



def makeCnf(bench, ilevel=0):
    print(os.getcwd())
    orig_config = '../cnf-orig/%s.cnf' % bench
    new_config = '../cnf/%s.cnf' % bench
    ofs = open(new_config, 'w')
    print("make cnf for ilevel", ilevel)

    ohaJar = 'oha-%s-i%d.jar' % (bench, ilevel)
    for line in open(orig_config):
        if line.startswith('  jars'):
            line = line.replace('  jars', '  jars "%s",' % ohaJar, 1)

        ofs.write(line)
    ofs.close()


if __name__ == "__main__":
    gg.useEA = 1
    # gg.java = path_to_openj9_java
    runj9(ils=2, n=30)
