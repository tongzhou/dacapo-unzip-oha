/*
 * Copyright (c) 2006, 2009 The Australian National University.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Apache License v2.0.
 * You may obtain the license at
 * 
 *    http://www.opensource.org/licenses/apache2.0.php
 */
import org.dacapo.harness.Callback;
import org.dacapo.harness.CommandLineArgs;
import java.io.PrintWriter;
import java.util.*;

/**
 * date:  $Date: 2009-12-24 11:19:36 +1100 (Thu, 24 Dec 2009) $
 * id: $Id: MyCallback.java 738 2009-12-24 00:19:36Z steveb-oss $
 */
public class OHACallback extends Callback {
  public static PrintWriter pw;
  public static int iter = 0;

  public OHACallback(CommandLineArgs args) {
    super(args);
    String fileName = "callbackCounts.txt";
    if (System.getenv("UseEA") != null) {
      fileName = "bb-callbackCounts.txt";
    }
    try {
      pw = new PrintWriter(fileName);
    } catch (Exception e) {
      e.printStackTrace();
      System.exit(1);
    }
    OHAHelper.installExitHandler();
  }

  /* Immediately prior to start of the benchmark */
  @Override
  public void start(String benchmark) {
    System.err.println("oha hook starting " + (isWarmup() ? "warmup " : "") + benchmark);
    super.start(benchmark);

    if (iter == 6) {
      OHAHelper.shouldElide = true;
      Arrays.fill(OHAHelper.localEscMap, true);
    }
  };

  /* Immediately after the end of the benchmark */
  @Override
  public void stop(long duration) {
    super.stop(duration);
    // System.err.println("oha hook stopped " + (isWarmup() ? "warmup" : ""));
    // System.err.flush();
  };

  @Override
  public void complete(String benchmark, boolean valid) {
    super.complete(benchmark, valid);
    System.err.println("oha hook " + (valid ? "PASSED " : "FAILED ") + (isWarmup() ? "warmup " : "") + benchmark);
    System.err.flush();
    System.err.println("Final counter: " + OHAHelper.counter);
    pw.println("Final counter: " + OHAHelper.counter);
    pw.flush();
    
    OHAHelper.counter = 0;
    iter += 1;
  };
}
